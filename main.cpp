#include <cstdlib>
#include <iostream>
#include <string>

//Determines if a player has won the game
int winOrLose(char *cross)
{
    if (cross[0] == 'X' && cross[1] == 'X' && cross[2] == 'X')
        return 1;
    else if (cross[0] == 'O' && cross[1] == 'O' && cross[2] == 'O')
        return 2;
    else if (cross[0] == 'X' && cross[3] == 'X' && cross[6] == 'X')
        return 1;
    else if (cross[0] == 'O' && cross[3] == 'O' && cross[6] == 'O')
        return 2;
    else if (cross[0] == 'X' && cross[4] == 'X' && cross[8] == 'X')
        return 1;
    else if (cross[0] == 'O' && cross[4] == 'O' && cross[8] == 'O')
        return 2;
    else if (cross[2] == 'X' && cross[4] == 'X' && cross[6] == 'X')
        return 1;
    else if (cross[2] == 'O'&& cross[4] == 'O' && cross[6] == 'O')
        return 2;
    else if (cross[1] == 'X' && cross[4]  == 'X' && cross[7] == 'X')
        return 1;
    else if (cross[1] == 'O'&& cross[4] == 'O'&& cross[7] == 'O')
        return 2;
    else if (cross[2] == 'X' && cross[5] == 'X' && cross[8] == 'X')
        return 1;
    else if (cross[2] == 'O' && cross[5] == 'O' && cross[8] == 'O')
        return 2;
}

void drawStart(char *cross, bool xO, int &p1, int &p2)
{
    std::cout << std::string(100, '\n');
    std::cout << "\tTic-Tac-Toe\n" << std::endl;
    std::cout << cross[0] << "_|_" << cross[1] << "_|_" << cross[2] << '\n';
    std::cout << cross[3] << "_|_" << cross[4] << "_|_" << cross[5] << '\n';
    std::cout << cross[6] << "_|_" << cross[7] << "_|_" << cross[8] << '\n';
    if (xO == true)
    {
        std::cout << "Player 1's cursor is X and Player 2's is O" << std::endl;
        p1 = 1;
        p2 = 0;
    }
    else
    {
        std::cout << "Player 1's cursor is O and Player 2's is X" << std::endl;
        p1 = 0;
        p2 = 1;
    }
}
// Generate a random number between min and max (inclusive)
// Assumes std::srand() has already been called
// Assumes max - min <= RAND_MAX
//This function wasn't made by me, I took it from www.learncpp.com
int getRandomNumber(int min, int max)
{
    static const double fraction = 1.0 / (RAND_MAX + 1.0);  // static used for efficiency, so we only calculate this value once
    // evenly distribute the random number across our range
    return min + static_cast<int>((max - min + 1) * (std::rand() * fraction));
}

int main()
{
    std::srand(static_cast<unsigned int>(time(0)));
    std::rand();
    //The whole board
    char cross[]
    {'1', '2', '3', '4', '5', '6', '7', '8', '9'};
    //Determines what cursor player 1 and player 2 should have randomly
    bool xO = static_cast<bool> (getRandomNumber(0, 1));
    int p1, p2;
    drawStart(cross, xO, p1, p2);
    int p1Numb, p2Numb;

    for (int count=1;count <= count; ++count)
    {
        //This is too insure that player 1 can't rewrite Player 2's move
        if ((count % 2) != 0)
        {
            std::cout << "Okay Player 1, Enter the number you'd like to replace" << std::endl;
            std::cin >> p1Numb;
            if (p1 == 1)
            {
                if (cross[p1Numb - 1] != 'O')
                    cross[p1Numb - 1] = 'X';
            }
            else
            {
                if (cross[p1Numb - 1] != 'X')
                    cross[p1Numb - 1] = 'O';
            }

        }
        //This is too insure that player 2 can't rewrite Player 1's move
        else
        {
            std::cout << "Okay Player 2, Enter the number you'd like to replace" << std::endl;
            std::cin >> p2Numb;
            if (p2 == 1)
            {
                if (cross[p2Numb - 1] != 'O')
                    cross[p2Numb - 1] = 'X';
            }
            else
            {
                if (cross[p2Numb - 1] != 'X')
                    cross[p2Numb - 1] = 'O';
            }
        }
        drawStart(cross, xO, p1, p2);
        if ((winOrLose(cross) == 1) && (p1 == 1))
        {
            std::cout << "Player 1 wins!" << std::endl;
            break;
        }

        else if ((winOrLose(cross) == 1) && (p2 == 1))
        {
            std::cout << "Player 2 wins!" << std::endl;
            break;
        }

        else if ((winOrLose(cross) == 2) && (p1 == 0))
        {
            std::cout << "Player 1 wins!" << std::endl;
            break;
        }

        else if ((winOrLose(cross) == 2) && (p2 == 0))
        {
            std::cout << "Player 2 wins!" << std::endl;
            break;
        }

        if (
            cross[0] != '1' &&
            cross[1] != '2' &&
            cross[2] != '3' &&
            cross[3] != '4' &&
            cross[4] != '5' &&
            cross[5] != '6' &&
            cross[6] != '7' &&
            cross[7] != '8' &&
            cross[8] != '9'
            )
                break;

    }
    return 0;
}
